Rewriting prevents modification of core system files.

This simple module demostrates how to rewrite method getChildren from core catalog/category model (Mage_Catalog_Model_Category class).

Read more: http://inchoo.net/magento/overriding-magento-blocks-models-helpers-and-controllers/
