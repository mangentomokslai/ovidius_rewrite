<?php
require_once 'app/Mage.php';

Mage::init();

// load category with id of 2
$category = Mage::getModel('catalog/category')->load(2);

// print child categories
print_r($category->getChildren());
