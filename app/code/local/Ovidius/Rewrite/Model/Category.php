<?php
class Ovidius_Rewrite_Model_Category extends Mage_Catalog_Model_Category
{
    public function getChildren() {
        // returns category ids in json format
        return json_encode($this->getResource()->getChildren($this, false));
    }
}
